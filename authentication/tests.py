from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class Login(TestCase):
    def test_url_login_is_exist(self):
        resp = Client().get('/accounts/login/')
        self.assertEqual(resp.status_code, 200)

    def test_login_template(self):
        resp = Client().get('/accounts/login/')
        self.assertTemplateUsed(resp, 'accounts/login.html')

    def test_logout_page_is_exist(self):
        resp = Client().get('/accounts/logout/')
        self.assertEqual(resp.status_code, 302)
    
    def test_logout_page_template(self):
        resp = Client().get('/accounts/logout/')

    def test_error_url(self):
        resp = Client().get('/accounts/masuk/')
        self.assertEqual(resp.status_code, 404)

    # Test Views
    def test_logout_page_using_func(self):
        found = resolve('/accounts/logout/')
        self.assertEqual(found.func, views.landing_logout)
    
    
    def test_login_page_using_func(self):
        found = resolve('/accounts/login/')
        self.assertEqual(found.func, views.signin)

    def test_login_logout(self):
        client = Client()

        username = 'test'
        password = 'bismillah123'
        user = User.objects.create_user(username=username, password=password)
        
        # Login
        response = client.post('/accounts/login/', {
            'username': username,
            'password': password
        })

        # Test if login successful
        response = client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('test', response.content.decode())
        
        # Logout
        response = client.get('/accounts/logout/')
