from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.conf import settings


def signup(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    if request.method == 'GET':
        form = UserCreationForm()
        return render(request, 'accounts/signup.html', {'form' : form})
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username = username, password = password)

            return HttpResponseRedirect('/accounts/login/')
        else:
            return render(request, 'accounts/signup.html', {'form' : form})



def signin(request):
    if request.user.is_authenticated:
        callback = request.META.get('HTTP_REFERER', '')
        host = request.get_host()
        if host not in callback or callback == host+request.path:
            callback = host
        return HttpResponseRedirect(callback)
    if request.method == 'GET':
        form = AuthenticationForm()
        return render(request, 'accounts/login.html', {'form' : form})
    if request.method == 'POST':
        form = AuthenticationForm(request = request, data = request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username = username, password = password)
           
            if user is not None:
                #print(user)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                pass
                # maunya pop up tulisan 'User not found'
                #print('Maunya pop up tulisan user not found')
        else:
            return render(request, 'accounts/login.html', {'form': form})

@login_required
def landing_logout(request):
    callback = request.META.get('HTTP_REFERER', '')
    host = request.get_host()
    if host not in callback:
        callback = host
    logout(request)
    return HttpResponseRedirect(callback)
