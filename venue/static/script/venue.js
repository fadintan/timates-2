$(document).ready(function(){

    getVenue();

    $('#refresh_venue').click(function(){
        getVenue();
    });

})

function getVenue() {
    $.ajax({
        url:'/venue/get_venue/',

        success:function (result) {
            if ($.trim($("#venue_list").html()).length != 0 ) {
                $("#venue_list").empty();
            }
            var venue_card = ""
            for (i = 0; i < result.content.length; i++) {
                var id = result.content[i].id;
                var name =  result.content[i].name;
                var image =  result.content[i].image;
                var short_address =  result.content[i].short_address;
                var price =  result.content[i].price;
                var category =  result.content[i].category;

                venue_card +=
                    "<div class='card venue-card m-2'>" + 
                        "<a href=" + id +">"+
                            "<img class='card-img-top venue-img' src='/" + image + "' alt='Venue Image'>"+
                            "<div class='card-body'>" +
                                "<h3 class='card-title venue-name text-info'>" + name + "</h3>" +
                                "<p class='card-text text-muted venue-location'>" + short_address + "</p>" +
                                "<p class='card-text text-muted venue-price'>Mulai dari <span class='text-danger'>Rp " + price + " </span></p>"+
                                "<p class='card-text text-muted venue-category'>" + category + "</p>" +
                            "</div> </a> </div>"
            }
            var no_venue = "<div class='alert alert-light' role='alert'>There's currently no venues in our database<br>You can submit your venue ^_^</div>";
            if (result.content.length == 0){
                $("#venue_list").html(no_venue);
            }else{
                $("#venue_list").html(venue_card);
            }
        }
    })
}