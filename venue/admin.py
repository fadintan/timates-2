from django.contrib import admin

# Register your models here.
from .models import Venue

admin.site.register(Venue)