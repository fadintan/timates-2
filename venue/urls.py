'''
URLs for Venue
'''
from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="venue_list"),
    path('<int:id>/', venue_page, name="venue_page"),
    path('create/', venue_form, name="venue_form"),
    path('get_venue/', get_venue, name="get_venue"),
]
