from django.http import JsonResponse
from django.utils.html import escape
from django.shortcuts import render, get_object_or_404, redirect
from .models import Venue
from .forms import VenueBookForm, CreateVenueForm
import json

# Create your views here.
def index(request):
    form = VenueBookForm(request.POST or None)
    if request.method == "POST" and form.is_valid:
            redirect('/findpartner/')
    response = {
        'venue_book_form' : form
    }
    return render(request, 'venue/list.html', response)

def venue_page(request, id):
    response = {
        'venue' : get_object_or_404(Venue, id=id),
    }
    return render(request, 'venue/page.html', response)

def venue_form(request):
    form = CreateVenueForm()
    if request.method == 'POST':
        form = CreateVenueForm(request.POST, request.FILES)
        print(request.FILES)
        if form.is_valid():
            form.save()
            return redirect("venue_list")
    response = {
        'form' : form
    }
    return render(request, 'venue/create.html', response)

def get_venue(request):
    response = {}
    if(request.method != 'GET'):
        response['status'] = 'failed'
        response['reason'] = 'invalid HTTP method'
        jsonResp = JsonResponse(response)
        jsonResp.status_code = 405
        return jsonResp
    venue = Venue.objects.all().values()
    venue_content = []
    for i in venue:
        venue_content.append({
            'id' : escape(i['id']),
            'name' : escape(i['name'].title()),
            'image' : 'media/' + i['image'], 
            'short_address' : escape(i['short_address'].title()), 
            'price' : '{:,}'.format((i['price'])), 
            'category' : escape(i['category']),
        })
    response['status'] = 'success'
    response['content'] = venue_content 
    jsonResp = JsonResponse(response)
    jsonResp.status_code = 200
    return jsonResp

    # <a href="{% url 'venue_page' venue.id %}">
    #                 <img class="card-img-top venue-img" src="{{ venue.image.url }}" alt="Venue Image">
    #                 <div class="card-body">
    #                     <h3 class="card-title venue-name text-info">{{ venue.name|title }}</h3>
    #                     <p class="card-text text-muted venue-location">{{ venue.short_address|title }}</p>
    #                     <p class="card-text text-muted venue-price">Mulai dari <span class="text-danger">Rp. {{ venue.price|intcomma }}</span></p>
    #                     <p class="card-text text-muted venue-category">{{ venue.category|title }}</p>
    #                 </div>
    #             </a>
