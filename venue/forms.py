from django import forms
from .models import Venue

class VenueBookForm(forms.Form):
    datetime = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'], widget=forms.DateTimeInput(attrs={'placeholder' : 'DD/MM/YYYY hh:mm'}))
    venue = forms.ModelChoiceField(queryset=Venue.objects.all().order_by('name'))

    def __init__(self, *args, **kwargs):
        super(VenueBookForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control bg-primary'
            visible.field.widget.attrs['autocomplete'] = 'off'


class CreateVenueForm(forms.ModelForm):
    # name = forms.CharField(max_length=50)
    # image = forms.ImageField()
    # address = forms.CharField(max_length=75)
    # short_address = forms.CharField(max_length=20)
    # open_hour = forms.TimeField()
    # close_hour = forms.TimeField()
    # price = forms.IntegerField(min_value=10000)
    # category = forms.CharField(max_length=20)
    # shower = forms.BooleanField(required=False)
    # locker = forms.BooleanField(required=False)
    # parking_space = forms.BooleanField(required=False)
    # food = forms.BooleanField(required=False)
    # wifi = forms.BooleanField(required=False)
    # shop = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super(CreateVenueForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control bg-primary mx-3'
            visible.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Venue
        fields = '__all__'