from django.test import TestCase, Client
from django.urls import resolve
from .views import index, venue_page
from .models import Venue
from .forms import VenueBookForm
from django.core.files.uploadedfile import SimpleUploadedFile
from django.apps import apps
from venue.apps import VenueConfig
from django.core.files.uploadedfile import SimpleUploadedFile


# Create your tests here.
class VenueTest(TestCase):
    # Sample image file
    small_gif = (
        b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
        b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
        b'\x02\x4c\x01\x00\x3b'
    )
    uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')

    # Models Test
    def create_venue(self, 
                    name="Futsal Field", address="Jakarta",image = uploaded,
                    short_address = 'Kalibata', open_hour = '07:00:00',close_hour = '22:00:00',price = 200000,
                    category = 'Sport',shower=True,locker = True,parking_space = True,food = True,wifi = True,shop = True,
                    ):
        return Venue.objects.create(name=name, address=address, image = image, short_address = short_address,
                    open_hour = open_hour, close_hour = close_hour,price = price, category = category, 
                    shower=shower,locker = locker,parking_space = parking_space,food = food,wifi = wifi,shop = shop,)

    #Views test
    def test_venue_creation(self):
        venue = self.create_venue()
        self.assertTrue(isinstance(venue, Venue))
        self.assertEqual(str(venue), venue.name)

    def test_venue_list_url_is_exist(self):
        response = Client().get('/venue/')
        self.assertEqual(response.status_code,200)

    def test_venue_using_venue_list_templates(self):
        response = Client().get('/venue/')
        self.assertTemplateUsed(response, 'venue/list.html')

    def test_venue_using_index_func(self):
        found = resolve('/venue/')
        self.assertEqual(found.func, index)

    #Test Venue Page
    def test_venue_page_url_is_exist(self):
        venue = self.create_venue()
        venue.save()
        response = Client().get('/venue/1/')
        self.assertEqual(response.status_code,200)

    def test_venue_page_using_venue_page_templates(self):
        venue = self.create_venue()
        venue.save()
        response = Client().get('/venue/1/')
        self.assertTemplateUsed(response, 'venue/page.html')

    def test_venue_page_using_venue_page_func(self):
        venue = self.create_venue()
        venue.save()
        found = resolve('/venue/1/')
        self.assertEqual(found.func, venue_page)

    #app.py test
    def test_apps(self):
        self.assertEqual(VenueConfig.name, 'venue')
        self.assertEqual(apps.get_app_config('venue').name, 'venue')

    