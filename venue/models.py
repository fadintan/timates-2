from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone

# Create your models here.
class Venue(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='venue_img/')
    address = models.CharField(max_length=75)
    short_address = models.CharField(max_length=20)
    open_hour = models.TimeField(default='07:00:00')
    close_hour = models.TimeField(default='22:00:00')
    price = models.IntegerField(default=10000)

    #Venue Type
    category = models.CharField(max_length=20)
    # field_type = models.TextField(max_length=20)
    # ground_type = models.TextField(max_length=20)


    #Facilities
    shower = models.BooleanField(default=False)
    locker = models.BooleanField(default=False)
    parking_space = models.BooleanField(default=False)
    food = models.BooleanField(default=False)
    wifi = models.BooleanField(default=False)
    shop = models.BooleanField(default=False)

    def __str__(self):
        return self.name

