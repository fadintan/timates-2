from django.shortcuts import render, redirect, get_object_or_404
from . import forms, models
from django.http import JsonResponse, HttpResponse
from django.utils.html import escape
from django.template import Template, RequestContext
import json
# Create your views here.
def champ(request):
    if request.method == "POST":
        form = forms.tournamentForm(request.POST)

        # if 'id' in request.POST:
        #     models.makeTournament.objects.get(id=request.POST['id']).delete()
        #     return redirect('/tournament/')

        if form.is_valid():
            tournamentData = models.makeTournament(
                event = form.data['event'],
                player = form.data['player'],
                location = form.data['location'],
                contact = form.data['contact'],
                date = form.data['date_year'] + "-" + form.data['date_month'] + "-" + form.data['date_day'],
                time = form.data['time'],
            )
            tournamentData.save()
            return redirect('/tournament/')
    else:
        form = forms.tournamentForm()

    tournament_context = {
        'tournament' : models.makeTournament.objects.all().values(),
        'form' : form,
    }

    return render(request, 'championship/index.html', tournament_context)

def land(request):
    return render(request, 'championship/landing.html')


def add_tournament(request):
    response = {'status':'', 'message':''}
    response_code = 0
    if(request.method == "POST"):
        champ_form = forms.tournamentForm(request.POST)
        if (form.is_valid()):
            if (request.user.is_authenticated):
                champ_form.save()

                response['status'] = "Success"
                response['message'] = "New tournament has been successfully created."
                response_code = 200
        else:
            response['status'] = "Failed"
            response['message'] = "Invalid data."
            response_code = 401
    else:
        response['status'] = "Failed"
        response['message'] = "Invalid HTTP method."
        response_code = 405

    jsonResp = JsonResponse(response)
    jsonResp.status.status_code = response_code
    return jsonResp


def get_tournament(request):
    response = {}
    if (request.method == 'POST'):
        print('masuk gan')
        form = forms.tournamentForm(request.POST)
        if form.is_valid():
            tournamentData = models.makeTournament(
                event = form.data['event'],
                sport = form.data['sport'],
                player = form.data['player'],
                location = form.data['location'],
                contact = form.data['contact'],
                date = form.data['date'],
                time = form.data['time'],
            )
            tournamentData.save()
        else:
            response = {
                'form' : 'formerr',
                'csrf' : Template('{% csrf_token %}').render(RequestContext(request)),
                'error' : form.as_p(),
            }
    tournaments = models.makeTournament.objects.all().values()
    details = []
    for i in tournaments:
        details.append({
            'event' : escape(i['event']),
            'sport' : escape(i['sport']),
            'player' : escape(i['player']),
            'location' : escape(i['location']),
            'contact' : escape(i['contact']),
            'date' : escape(i['date']),
            'time' : escape(i['time']),
        })
    response['status'] = 'success'
    response['details'] = details
    jsonResp = JsonResponse(response)
    jsonResp.status_code = 200
    return jsonResp