$(document).ready(function(){
    $('#int').submit(function(event){
        event.preventDefault()
        addTournament();
    });
})

function addTournament(){
    $.ajax({
        url:'/tournament/get_tournament/',
        method: "POST",
        data:{
            "event" : $("#id_event").val(),
            "sport" : $("#id_sport").val(),
            "player" : $("#id_player").val(),
            "location" : $("#id_location").val(),
            "contact" : $("#id_contact").val(),
            "date" : $("#id_date_year").val() + "-" + $("#id_date_month").val() + "-" + $("#id_date_day").val(),
            "time" : $("#id_time").val(),
            "csrfmiddlewaretoken" : $("input[name='csrfmiddlewaretoken']").val(),
        },
        success: function(data){
            console.log(data)
            if (data.form == "formerr")
            {
                $("#int").empty()
                var form_and_csrf = data.error + data.csrf + `<button type="submit" id="submit_tournament" style="background: rgb(255, 232, 115);" value="Submit" class="btn btn-light">Submit</button>`
                $("#int").html(form_and_csrf)
            }
            $("#games").empty()
            $.each(data.details, function(i, items){
                var tmpintan = ``
                tmpintan += `            <div class="d-flex align-items-center justify-content-center">
                <div>
                    <img src="/static/img/trophy.png" alt="BD" width=135px>
                </div>
                <div class="d-flex flex-column align-items-center justify-content-center each-game">
                    <!--utk detail turnamen-->
                    <div class="d-flex align-content-center">
                        <p class="text-center">
                        <strong>` + items.event + `</strong><br>  
                        Person needed: ` + items.payer + `<br>` + 
                        items.location + `<br>` +
                        items.contact +`<br>` +
                        items.date + `<br>` +
                        items.time + `<br>
                        </p>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="/tournament/landing/">
                            <button type="button" class="btn btn-light mx-auto mt-2" style="background: rgb(255, 232, 115); height: 42px">Join</button>
                        </a>  
                    </div>

                </div> 
                </div>`

                console.log(items)
                $("#games").append(tmpintan)

            })
        }
    })
}