from django.urls import path
from . import views

urlpatterns = [
    path('', views.champ, name='championship'),
    path('landing/', views.land, name='landing'),
    path('get_tournament/', views.get_tournament, name="get_tournament"),
]