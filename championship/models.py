from django.db import models

# Create your models here.
class makeTournament(models.Model):
    event = models.CharField(max_length = 100)
    sport = models.CharField(max_length = 50)
    player = models.IntegerField(default = 0) # cek spy dia ga <2
    location = models.CharField(max_length = 50)
    contact = models.CharField(max_length = 50)
    date = models.DateField()
    time = models.TimeField()