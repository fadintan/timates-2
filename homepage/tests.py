from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from django.apps import apps
from homepage.apps import HomepageConfig


# Create your tests here.
class HomePage(TestCase):
    #Views tesT
    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_homepage_using_homepage_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage/index.html')

    def test_homepage_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    #app.py test
    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

