from django import forms

from .models import PostModel

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = [
            'game_title',
            'game_type',
            'slot',
            'location',
            'date',
            'time',
            'contact',
        ]
        labels = {
            'game_title':'Game Title',
            'game_type':'Sport',
            'location':'Location',
            'date':'Date',
            'slot':'Slot',
            'time':'Time',
            'contact':'Contact Person'
        }

        widgets = {
            'game_title':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Fun Sport'
                }
            ),
            'date':forms.DateInput(
                attrs = {
                    'type' : 'date',
                    'class':'form-control',
                }
            ),
            'time':forms.TimeInput(
                attrs = {
                    'type' : 'time',
                    'class':'form-control',
                }
            ),
            'location':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Sport Venue'
                }
            ),
            'slot':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'20'
                }
            ),
            'game_type':forms.Select(
                attrs = {
                    'class':'form-control'
                }
            ),
            'contact':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'0812xxxxxxx (Name)'
                }
            ),
        }
