from django.urls import path
from . import views

urlpatterns = [
    path('', views.find, name='findpartner'),
    path('add-game/', views.add_game, name='addgame'),
    path('get-games/<str:query>', views.get_game, name='getgame'),
    path('success/', views.success, name='success'),
]
