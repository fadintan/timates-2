from django.db import models
import datetime

# Create your models here.

class PostModel(models.Model):
    game_title = models.CharField(max_length=100)
    
    LIST_CATEGORY = (
        ('Badminton', 'Badminton'),
        ('Baseball', 'Baseball'),
        ('Basketball', 'Basketball'),
        ('Bowling', 'Bowling'),
        ('Futsal', 'Futsal'),
        ('Pingpong', 'Pingpong'),
        ('Tennis', 'Tennis')
    )
    
    game_type = models.CharField(max_length=100,
        choices = LIST_CATEGORY, default = 'Futsal')
    slot = models.IntegerField(default=0)
    location = models.CharField(max_length=100)
    date = models.DateField(default = datetime.date.today)
    time = models.TimeField(default = '00:00')
    contact = models.CharField(max_length=100)
    
    def __str__(self):
        return "{}.{}".format(self.id, self.game_title)