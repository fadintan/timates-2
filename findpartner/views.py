from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.utils.html import escape
from .forms import PostForm
from .models import PostModel
import json

def find(request):
    post_form = PostForm(request.POST or None)
    if request.method == "POST":
        if post_form.is_valid():
            post_form.save()
            return redirect('/findpartner/')
    else:
        post_form = PostForm()
            
    posts = PostModel.objects.all()
    contexts = {
        'posts': posts,
        'form': post_form,
    }
    return render(request, 'findpartner/index.html', contexts)

def add_game(request):
    response = {'status':'', 'message':''}
    response_code = 0
    if(request.method == "POST"):
        post_form = PostForm(request.POST)
        if (form.is_valid()):
            if (request.user.is_authenticated):
                post_form.save()

                response['status'] = "Success"
                response['message'] = "New game has been successfully created."
                response_code = 200
        else:
            response['status'] = "Failed"
            response['message'] = "Invalid data."
            response_code = 401
    else:
        response['status'] = "Failed"
        response['message'] = "Invalid HTTP method."
        response_code = 405

    jsonResp = JsonResponse(response)
    jsonResp.status.status_code = response_code
    return jsonResp

def get_game(request, query):
    url = '/findpartner/get-game?q=' + query
    response = {}
    if(request.method != 'GET'):
        response['status'] = 'Failed'
        response['message'] = 'Invalid HTTP method.'
        jsonResp = JsonResponse(response)
        jsonResp.status_code = 405
        return jsonResp
    posts = PostModel.objects.all().values()
    game_details = []
    for i in posts:
        game_details.append({
            'game_title' : escape(i['game_title']),
            'game_type' : escape(i['game_type']),
            'slot' : escape(i['slot']),
            'location' : escape(i['location']),
            'date' : escape(i['date']),
            'time' : escape(i['time']),
            'contact' : escape(i['contact']),
        })
    response['status'] = 'success'
    response['details'] = game_details
    jsonResp = JsonResponse(response)
    jsonResp.status_code = 200
    return jsonResp

def success(request):
    return render(request, 'findpartner/landing.html')
