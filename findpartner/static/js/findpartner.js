$(document).ready(function(){
	$('#search-button').click(function(){
		var query = $('#search-box').val();

		$(function(){
			$.ajax({
				url: "/findpartner/get-games/" + query,
				success: function(result){
					$('#games-table').empty();		
				    $.each(result.details, function(i, item) {
				        $('<tr>').append(
				        	$('<th scope="row">').text((i+1)),
                            $('<td>').text(item.game_title),
                            $('<td>').text(item.game_type),
                            $('<td>').text(item.date),
                            $('<td>').text(item.time),
                            $('<td>').text(item.slot),
                            $('<td>').text(item.location),
                            $('<td>').text(item.contact),
                            $('<td>').append('<a href="success/" style="color: black;"><strong>Join</strong></a>'),
				        ).appendTo('#games-table');
					});
				}
			});
		});
	});
	$('#search-box').val("basketball");
	$('#search-button').click();
});
