[![pipeline status](https://gitlab.com/fadintan/timates-2/badges/master/pipeline.svg)](https://gitlab.com/fadintan/timates-2/commits/master)
[![coverage report](https://gitlab.com/fadintan/timates-2/badges/master/coverage.svg)](https://gitlab.com/fadintan/timates-2/commits/master)

# TK2 PPW - KB04

1. Fatih Ismail Alaydrus    (1806146953)
2. Intan Fadilla A.         (1806141246)
3. Kirana Alfatianisa       (1806186843)        
4. Muhammad Aulia Akbar     (1806133805)

## Link Herokuapp
[Timates](https://timates.herokuapp.com/)

## About
Timates adalah sebuah platform yang membantu para pecinta olahraga untuk menemukan partner berolahraga. Selain mempertemukan, Timates juga mempermudah proses booking venue olahraga dengan menyematkan sistem yang terintegrasi. Timates hadir untuk menjawab semua kebutuhan pencinta olahraga.

## Features
1. Mencari partner berolahraga (Kirana A.),
2. Booking venue olahraga terintegrasi (Fatih Ismail A.),
3. Blog dan berita seputar olahraga (M. Aulia Akbar) , 
4. Turnamen atau pertandingan antar komunitas yang bisa diadakan oleh siapapun (Intan Fadilla A.).