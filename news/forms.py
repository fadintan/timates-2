from django import forms
from .models import Blog

class AddBlogForm(forms.Form):
    title_attrs = {
        'type':'text',
        'class': 'form-control bg-secondary',
        'placeholder': 'title'
    }

    content_attrs = {
        'type':'text',
        'class': 'form-control bg-secondary',
        'placeholder': 'start writing awesome stories...'
    }

    title = forms.CharField(label='Title', max_length=50,  required=True, widget=forms.TextInput(attrs=title_attrs))
    content = forms.CharField(label='Content', required=True, widget=forms.Textarea(attrs=content_attrs))