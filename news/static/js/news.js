$(document).ready(function(){

    show(); 
    getNews(); 
    getBlog();

    $('#refresh_blog').click(function(){
        getBlog()
    });

    $('#submit_blog').click(function(){
        addBlog()
    });

})

function getNews() {
    $.ajax({
        url:'/news/get-news/',

        success:function (result) {
            if ($.trim($("#news-content").html()).length != 0 ) {
                $("#news-content").empty();
            }
            var complete_text = ""
            for (i = 0; i < result.totalResults; i++) {
                var url = result.articles[i].url;
                var urlToImage =  result.articles[i].urlToImage;
                var title =  result.articles[i].title;
                var description =  result.articles[i].description;

                complete_text +=
                '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 pt-3 pb-3">'+
                    '<div class="hovereffect">'+
                    '<a href="' + url + '">'+
                            '<img class="" src="' + urlToImage + '" alt="">'+
                                '<div class="overlay">'+
                                    '<h2>' + title +'</h2>'+
                                    '<p>'+ description + '</p>'+
                                '</div>'+                    
                        '</a>'+
                    '</div>'+
                '</div>';
            }
            var container_open = '<div class="d-flex justify-content-around align-items-center flex-wrap">';
            var container_close = '</div> <div class="text-center"><p>powered by <a class="text-dark font-weight-bold" href="https://newsapi.org">newsapi.org</a></p></div>';
            $("#news-content").html(container_open+ complete_text + container_close);
        }
    })
}

function getBlog() {
    $.ajax({
        url:'/news/get-blog/',
        success:function (result) {
            if ($.trim($("#blog-content").html()).length != 0 ) {
                $("#blog-content").empty();
            }
            var complete_text = ""
            for (i = 0; i < result.blog_length; i++) {
                var author = result.content[i].author;
                var title =  result.content[i].title;
                var content =  result.content[i].content;
                var posting_time =  result.content[i].posting_time;

                complete_text += 
                '<div class="mb-3">' +
                    '<h3 class="font-weight-bold">' +
                        title +
                    '</h3>' +
                    '<h4>' +
                        'author : ' + author +
                    '</h4>' +
                    '<p>' +
                        content +
                    '</p>' +
                    '<h6>' +
                        'posted at ' + posting_time +
                    '</h6>' +
                '</div>';

            }
            if (result.blog_length == 0){
                complete_text = '<h2>be the first to create blog.</h2>';
            }
            $("#blog-content").html(complete_text);
        }
    })
}

function addBlog() {
    var title = $("#id_title").val();
    var content = $("#id_content").val();
    $.ajax({
        type: "POST",
        url:'/news/add-blog/',
        data:{
            'title': title,
            'content': content,
            'csrfmiddlewaretoken': getCookie('csrftoken')
        },
        success:function (result) {
            $('#id_title' ).val("");    
            $('#id_content' ).val("");      
            $("#addStatus").html(result.messages)
            $("#addStatusPopup").modal();
            getBlog();
        },
        error: function (jqXHR, status, err) {
            $("#addStatus").html(jqXHR.responseJSON.messages)
            $("#addStatusPopup").modal();            
        }
    })
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function show() {
    $('.jsonly').css('display','block');
}