from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='news'),
    path('get-news/', views.getNews, name='get-news'),
    path('get-blog/', views.getBlog, name='get-blog'),
    path('add-blog/', views.addBlog, name='add-blog')
]