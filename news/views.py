from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.utils.html import escape
from .models import Blog
from .forms import AddBlogForm
import requests
from datetime import datetime
import json


def index(request):
    response = {}
    response['add_blog_form'] = AddBlogForm
    return render(request, 'news/index.html', response)

def getBlog(request):
    response = {}
    if(request.method != 'GET'):
        response['status'] = 'failed'
        response['reason'] = 'invalid HTTP method'
        jsonResp = JsonResponse(response)
        jsonResp.status_code = 405
        return jsonResp
    blog = Blog.objects.all().values()
    response['blog_length'] = blog.count()
    blog_content = []
    for i in blog:
        blog_content.append({
            'author' : escape(i['author']), 
            'title' : escape(i['title']), 
            'content' : escape(i['content']), 
            'posting_time' : escape(i['posting_time'].strftime("%d %B, %Y"))
        })
    response['status'] = 'success'
    response['content'] = blog_content 
    jsonResp = JsonResponse(response)
    jsonResp.status_code = 200
    return jsonResp

def getNews(request):
    response = {}
    responseCode = 0
    if(request.method != 'GET'):
        response['status'] = 'failed'
        response['reason'] = 'invalid HTTP method'
        jsonResp = JsonResponse(response)
        responseCode = 405
        jsonResp.status_code = responseCode
        return jsonResp
    url = 'https://newsapi.org/v2/top-headlines?'
    source = 'sources=espn&' #fox-sport espn
    apiKey = 'apiKey=' + settings.NEWS_API_KEY
    response_api = requests.get(url+source+apiKey)
    if (response_api.status_code==200 and response_api.json()['status']=='ok'):
        resp = response_api.json()
        response = resp
        response['status'] = 'success'
        responseCode = 200
    else:
        placeholder = {'url':"#", 
        'urlToImage':"https://webhostingmedia.net/wp-content/uploads/2016/12/500-internal-server-error.png", 
        'title':'500 internal server error', 
        'description':'there is something wrong while trying to retreive news, please try again later.',
        }
        response = {'articles' : [placeholder, placeholder, placeholder]}       
        response['status'] = 'failed'  
        responseCode = 500
    jsonResp = JsonResponse(response)
    jsonResp.status_code = responseCode
    return jsonResp

def addBlog(request):
    response = {'save_status':'', 'message':''}
    responseCode = 0
    if(request.method == 'POST'):
        form = AddBlogForm(request.POST)
        if (form.is_valid()):
            if (request.user.is_authenticated):
                author = request.user.username
                title = form.cleaned_data['title']
                content = form.cleaned_data['content']
                
                blog = Blog(
                    author = author,
                    title = title,
                    content = content,
                )

                blog.save()

                response['save_status'] = 'success'
                response['messages'] = 'blog created succesfully :)'
                responseCode = 200
            else:
                response['save_status'] = 'failed'
                response['messages'] = 'user is unauthenticated' 
                responseCode = 401               
        else:
            response['save_status'] = 'failed'
            response['messages'] = 'invalid data'
            responseCode = 400
    else:
        response['save_status'] = 'failed'
        response['messages'] = 'invalid HTTP method'     
        responseCode = 405

    jsonResp = JsonResponse(response)
    jsonResp.status_code = responseCode
    return jsonResp