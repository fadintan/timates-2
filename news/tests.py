from django.test import TestCase, Client
from django.utils import timezone
from django.contrib.auth.models import User
from django.conf import settings
from django.apps import apps
from .views import index, addBlog
from .models import Blog
from .apps import NewsConfig


class NewsTest(TestCase):
    #test models
    def create_blog(self, 
                    author="John Doe", title = "Has seraphina regain her power??",
                    content = '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ut lectus turpis. Proin dapibus vestibulum justo, pharetra consectetur odio aliquam non. Nam vestibulum, erat id auctor dignissim, leo nunc laoreet nisi, vitae fermentum risus dolor ac libero. Aenean euismod elit nisi, ac fringilla magna fermentum quis. Donec dui metus, ultrices sit amet hendrerit quis, pharetra a lectus. Aliquam erat volutpat. Etiam at pellentesque massa, at fermentum neque. Vestibulum sagittis metus non libero finibus, finibus venenatis ex scelerisque. Nam pretium ut arcu lobortis faucibus. Duis quis libero suscipit, lobortis massa sed, interdum leo. Aenean faucibus, neque vel lobortis lobortis, nulla risus tempor est, a sodales augue purus non nibh. Etiam convallis vitae libero at egestas. Curabitur et lorem eros. Donec in ligula orci. Suspendisse porta mi non mauris rutrum feugiat. Curabitur efficitur leo sit amet nibh euismod, sit amet cursus turpis rhoncus.

Pellentesque ut dignissim nunc, at finibus dui. In laoreet leo nisi, eu egestas quam consequat in. Fusce id aliquet justo. Vestibulum pretium elit vitae est sodales interdum ut sed sapien. Duis semper bibendum nulla vel vestibulum. Phasellus sollicitudin nunc at dui pulvinar suscipit. Cras pulvinar mauris sit amet rhoncus venenatis. Cras euismod felis et quam semper, at ornare metus rutrum. Praesent sed lacus at magna tempus tristique ut a nisi. Vivamus venenatis sollicitudin massa, nec sodales lorem tristique at. Integer mattis metus et accumsan semper.

Sed ac varius neque, in blandit turpis. Suspendisse viverra scelerisque nulla, ac viverra elit tempus vel. Etiam semper urna ut dapibus semper. Aliquam lacus nibh, bibendum sed feugiat eget, fermentum aliquam tortor. Nullam a ligula sed tortor mollis blandit quis eget turpis. Donec auctor ante nec dolor commodo sollicitudin. Integer eu volutpat metus. Duis vehicula auctor neque, vitae aliquam metus ullamcorper quis. Phasellus lacinia, lacus eu tristique maximus, lorem justo sodales nisi, eu ultrices massa sem ut mauris. Donec dictum venenatis lorem vitae dictum. Mauris a nibh ut quam suscipit ultricies. Phasellus id interdum neque. Etiam nunc ipsum, viverra sed nibh ac, scelerisque volutpat mauris. Donec eget fringilla dolor. Cras ut finibus metus.

Pellentesque sodales luctus turpis, ac bibendum diam dapibus vestibulum. Suspendisse potenti. Cras efficitur tempor fringilla. Integer faucibus sem eget dui semper, a varius eros bibendum. Donec ac semper mi. Morbi imperdiet sed sapien in varius. Donec posuere eget tellus sed aliquam. Fusce ac orci eu orci tristique auctor. Sed at augue enim. Curabitur sollicitudin tortor non nisi tristique, vitae tincidunt libero facilisis. Praesent at finibus leo, a sollicitudin diam. Aliquam quis dui velit. Quisque venenatis dolor ut lacus tempus elementum id vel libero.

Sed in viverra sapien, eget sagittis eros. Etiam fringilla urna tellus, ornare dictum est eleifend sit amet. Pellentesque pulvinar, diam et ornare interdum, metus mauris posuere eros, eu tristique neque mauris vel nulla. Aenean a maximus mi, in fermentum libero. Duis et ligula in orci viverra sodales sed ut dolor. Integer purus nulla, gravida sed sollicitudin et, maximus sed sem. Sed sed enim eget arcu sodales facilisis. Morbi ac justo id tellus tincidunt aliquet quis id nulla. Nullam vitae scelerisque augue. Cras vitae risus nulla. Aliquam sit amet massa mi. Mauris bibendum finibus orci.'''):
        return Blog.objects.create(author=author, title=title, content = content)

    def test_blog_creation(self):
        blog = self.create_blog()
        self.assertTrue(isinstance(blog, Blog))

    #test urls
    def test_news_is_using_templates(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news/index.html')
        self.assertEqual(response.status_code,200)               



    #test get-blog
    def test_get_blog_wrong_http_request(self):
        response = Client().post('/news/get-blog/')
        self.assertIn('failed', response.content.decode())
        self.assertEqual(response.status_code,405)     


    def test_get_blog_url_is_exist(self):
        blog = self.create_blog()
        response = Client().get('/news/get-blog/')
        self.assertIn('success', response.content.decode())
        self.assertIn('John Doe', response.content.decode())
        self.assertEqual(response.status_code,200)     



    #test get-news
    def test_get_news_wrong_http_request(self):
        response = Client().post('/news/get-news/')
        self.assertIn('failed', response.content.decode())
        self.assertEqual(response.status_code,405)         

    def test_get_news_url_is_exist(self):
        response = Client().get('/news/get-news/')
        self.assertIn('success', response.content.decode())
        self.assertEqual(response.status_code,200)         



    #test add-blog
    def test_Blog_creation_wrong_http_request(self):
        client = Client()

        response = client.get('/news/add-blog/')
        self.assertEqual(response.status_code, 405)
        self.assertIn("failed", response.content.decode())
        self.assertNotIn("success", response.content.decode())


    def test_Blog_creation_invalid(self):
        client = Client()

        username = 'test'
        password = 'bismillah123'
        user = User.objects.create_user(username=username, password=password)

        response = client.post('/login/', {
            'username': username,
            'password': password
        })

        response = client.post('/news/add-blog/')
        self.assertEqual(response.status_code, 400)
        self.assertIn("failed", response.content.decode())
        self.assertNotIn("success", response.content.decode())


    def test_Blog_creation_unauthenticated(self):
        client = Client()

        response = client.post('/news/add-blog/', data={"title" : "Has seraphina regain her power??", "content" : "cya in season 2 :v"})
        self.assertEqual(response.status_code, 401)
        self.assertNotIn("success", response.content.decode())
        self.assertIn("failed", response.content.decode())


    def test_Blog_creation_valid(self):
        client = Client()

        username = 'test'
        password = 'bismillah123'
        user = User.objects.create_user(username=username, password=password)
        
        response = client.post('/accounts/login/', {
            'username': username,
            'password': password
        })

        response = client.post('/news/add-blog/', data={"title" : "Has seraphina regain her power??", "content" : "cya in season 2 :v"})
        self.assertEqual(response.status_code, 200)
        self.assertIn("success", response.content.decode())
        self.assertNotIn("failed", response.content.decode())



    # test config
    def test_NewsConfig(self):
        self.assertEqual(NewsConfig.name, 'news')
        self.assertEqual(apps.get_app_config('news').name, 'news')

